import com.cruzeteam.parser.entity.EntityContainer;
import com.cruzeteam.parser.network.DefaultHttpClientsPool;
import com.cruzeteam.parser.network.IHttpClientsPool;
import com.cruzeteam.parser.web.Parser;
import org.junit.Before;
import org.junit.Test;
import taganrok.web.allorostov.AllorostovItemsParser;
import taganrok.web.allorostov.AllorostovPagesParser;

import java.net.URI;
import java.util.List;

/**
 * Created by Алексей Анисов on 10/08/15.
 * © TomskSoft
 */
public class AllorostovItemsParserTest {
    Parser parser;
    EntityContainer container;

    @Before
    public void setup() throws Exception {
        parser = new AllorostovItemsParser();
        container = new EntityContainer();
        parser.setContainer(container);
        IHttpClientsPool pool = new DefaultHttpClientsPool();
        parser.setHttpClientsPool(pool);
    }

    @Test
    public void test() {
        assert parser.testLink("http://allorostov.ru/yp_5000/yp_5011/yp_6657/?page=1");
        parser.parseEntity(URI.create("http://allorostov.ru/yp_5000/yp_5011/yp_6657/?page=1"));
        container.stream().map(s -> s.toString()).forEach(System.out::println);
    }
}
