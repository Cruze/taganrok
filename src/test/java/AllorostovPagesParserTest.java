import com.cruzeteam.parser.entity.EntityContainer;
import com.cruzeteam.parser.network.DefaultHttpClientsPool;
import com.cruzeteam.parser.network.IHttpClientsPool;
import com.cruzeteam.parser.web.Parser;
import org.junit.Before;
import org.junit.Test;
import taganrok.web.allorostov.AllorostovMainPageParser;
import taganrok.web.allorostov.AllorostovPagesParser;

import java.net.URI;
import java.util.List;

/**
 * Created by Алексей Анисов on 10/08/15.
 * © TomskSoft
 */
public class AllorostovPagesParserTest {
    Parser parser;

    @Before
    public void setup() throws Exception {
        parser = new AllorostovPagesParser();
        parser.setContainer(new EntityContainer());
        IHttpClientsPool pool = new DefaultHttpClientsPool();
        parser.setHttpClientsPool(pool);
    }

    @Test
    public void test() {
        assert parser.testLink("http://allorostov.ru/yp_5000/yp_5001/yp_6667/");
        List<URI> links = parser.parseLinks(URI.create("http://allorostov.ru/yp_5000/yp_5001/yp_6667/"));
        links.forEach(System.out::println);
    }
}
