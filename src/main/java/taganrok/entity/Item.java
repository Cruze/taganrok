package taganrok.entity;

import com.cruzeteam.parser.entity.IEntity;

/**
 * Created by Cruze on 20.07.2015.
 * Item
 */
public class Item implements IEntity {

    private String name = "";

    private String activity = "";

    private String phone = "";

    private String adress = "";

    public Item(String name, String activities, String phone, String adress) {
        this.name = name;
        this.activity = activities;
        this.phone = phone;
        this.adress = adress;
    }

    public Item() {
    }

    public String getName() {
        return name;
    }

    public String getActivity() {
        return activity;
    }

    public String getPhone() {
        return phone;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", activity='" + activity + '\'' +
                ", phone='" + phone + '\'' +
                ", adress='" + adress + '\'' +
                '}' + '\n' ;
    }
}
