package taganrok.out;


import com.cruzeteam.parser.entity.IEntity;
import com.cruzeteam.parser.out.Out;
import taganrok.entity.Item;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Cruze on 10.06.2015.
 * excel out result of parsing
 */
public class ExcelOut extends Out {

    @Override
    public boolean toFile(String filename) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        createFirstRow(sheet);

        int rowCount = 1;

        for (IEntity entity : _entities) {
            Item item = (Item) entity;
            Row row = sheet.createRow(rowCount++);
            fillRow(item, row);
        }

        try(FileOutputStream out = new FileOutputStream(new File(filename))) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void fillRow(Item item, Row row) {
        Cell cell;
        cell = row.createCell(0);
        cell.setCellValue(item.getName());

        cell = row.createCell(1);
        cell.setCellValue(item.getActivity());

        cell = row.createCell(2);
        cell.setCellValue(item.getPhone());

        cell = row.createCell(3);
        cell.setCellValue(item.getAdress());
    }


    private void createFirstRow(Sheet sheet) {
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellValue("Название");

        cell = row.createCell(1);
        cell.setCellValue("Сфера деятельности");

        cell = row.createCell(2);
        cell.setCellValue("Адрес");

        cell = row.createCell(3);
        cell.setCellValue("Телефон");
    }

    public ExcelOut(List<IEntity> entities) {
        super(entities);
    }
}
