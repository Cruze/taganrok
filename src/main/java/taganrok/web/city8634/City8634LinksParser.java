package taganrok.web.city8634;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze on 20.07.2015.
 * for links
 */
public class City8634LinksParser extends Parser {

    public static final String REGEX = "http://www.8634city.ru/catalog/page/1000";

    public static final String URL = "http://www.8634city.ru/catalog/page/";

    public City8634LinksParser() {
        super(REGEX);
    }

    @Override
    public List<URI> parseLinks(URI url) {
        List<URI> result = new ArrayList<>();
        Document doc = getJsoupDocument(url);

        String string = doc.select("div.center div.inner-title span").text();
        int countPage = Integer.parseInt(string.replaceAll("\\D", ""));

        for (int i = 1; i <= countPage; i++) {
            result.add(URI.create(URL + i));
        }

        return result;
    }

}
