package taganrok.web.city8634;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import taganrok.entity.Item;

import java.net.URI;

/**
 * Created by Cruze on 20.07.2015.
 * for item
 */
public class City8634ItemsParser extends Parser {

    private static final String REGEX = "http://www.8634city.ru/catalog/page/\\d+";

    public City8634ItemsParser() {
        super(REGEX);
    }

    @Override
    public void parseEntity(URI uri) {
        Document doc = getJsoupDocument(uri);

        Elements elements = doc.select("div.conteiner div.company_box");

        for (Element element : elements) {
            getContainer().add(new Item(
                    getName(element),
                    getActivity(element),
                    getPhone(element),
                    getAdress(element)
            ));
        }
    }

    private String getName(Element element) {
        return element.select("div.company_title").text();
    }

    private String getActivity(Element element) {
        return element.select("div.stat_company p.small_gray_text a[class=small_gray_text rubric]").text();
    }

    private String getPhone(Element element) {
        Elements elements = element.select("div[class=contacts gray_box rounding");

        if (elements.size() <= 1)
            return "";

        return element.select("div[class=contacts gray_box rounding").get(1).text();
    }

    private String getAdress(Element element) {

        Elements elements = element.select("div[class=contacts gray_box rounding]");

        if (elements.size() <= 1){
            return "";
        }

        return element.select("div[class=contacts gray_box rounding]").get(0).text();
    }

}
