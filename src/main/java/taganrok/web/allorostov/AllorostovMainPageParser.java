package taganrok.web.allorostov;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей Анисов on 10/08/15.
 * © TomskSoft
 */
public class AllorostovMainPageParser extends Parser {
    private static final String REGEXP = "http://allorostov.ru/";
    private static final String PREFIX = "http://allorostov.ru";

    public AllorostovMainPageParser() {
        super(REGEXP);
    }

    @Override
    public List<URI> parseLinks(URI url) {
        List<URI> result = new ArrayList<>();
        Document doc = getJsoupDocument(url);

        Elements titles = doc.select(".title a");

        for (Element title : titles) {
            result.add(URI.create(PREFIX + title.attr("href")));
        }

        return result;
    }
}
