package taganrok.web.allorostov;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import taganrok.entity.Item;

import java.net.URI;

/**
 * Created by Алексей Анисов on 10/08/15.
 * © TomskSoft
 */
public class AllorostovItemsParser extends Parser {
    private static final String REGEXP = "http://allorostov.ru/yp_\\d+/yp_\\d+(/yp_\\d+)?/\\?page=\\d+";

    public AllorostovItemsParser() {
        super(REGEXP);
    }

    @Override
    public void parseEntity(URI url) {
        Document doc = getJsoupDocument(url);

        Elements rows = doc.select(".info tr");

        try {
            Item item = null;
            String title = "";
            String activity = new String(doc.select(".text-way h1").text().getBytes("CP1252"), "CP1251");
            for (Element row : rows) {
                Elements columns = row.children();
                switch (columns.first().attr("colspan")) {
                    default:
                        if (columns.size() == 2) {
                            if (columns.get(0).hasClass("comm"))
                                continue;
                            String key = new String(columns.get(0).text().getBytes("CP1252"), "CP1251");
                            key = key.substring(0, key.length() - 1);
                            String value = new String(columns.get(1).text().getBytes("CP1252"), "CP1251");
                            switch (key) {
                                case "Адрес":
                                    if (item != null)
                                        getContainer().add(item);

                                    item = new Item();
                                    item.setName(title);
                                    item.setActivity(activity);
                                    item.setAdress(value);
                                    break;
                                case "Телефон":
                                    if (item != null)
                                        item.setPhone(value);
                                    break;
                            }
                        }
                        break;
                    case "2":
                        Elements b = columns.first().select("b");
                        if (!b.isEmpty()) {
                            title = new String(b.text().getBytes("CP1252"), "CP1251");
                        }
                        break;
                }
            }
            if (item != null)
                getContainer().add(item);
        }
        catch (Exception e) {
            System.out.println("link: " + url.toString());
            e.printStackTrace();
        }
    }
}
