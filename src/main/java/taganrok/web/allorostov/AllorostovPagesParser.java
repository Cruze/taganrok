package taganrok.web.allorostov;

import com.cruzeteam.parser.web.Parser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей Анисов on 10/08/15.
 * © TomskSoft
 */
public class AllorostovPagesParser extends Parser {
    private static final String REGEXP = "http://allorostov.ru/yp_\\d+/yp_\\d+/(yp_\\d+/)?";

    public AllorostovPagesParser() {
        super(REGEXP);
    }

    @Override
    public List<URI> parseLinks(URI url) {
        List<URI> result = new ArrayList<>();
        Document doc = getJsoupDocument(url);

        Element pages = doc.select(".info td").last();
        try {
            String text = new String(pages.text().getBytes("CP1252"), "CP1251");

            if (text.contains("Страницы")) {
                int lastPage = Integer.parseInt(pages.select("a").last().text());
                for (int i = 1; i <= lastPage; i++) {
                    result.add(URI.create(url.toString() + "?page=" + i));
                }
            }
            else {
                result.add(URI.create(url.toString() + "?page=1"));
            }
        }
        catch (Exception e) {
            System.out.println("link: " + url.toString());
            e.printStackTrace();
        }

        return result;
    }
}
