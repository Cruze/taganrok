package taganrok.web;

import com.cruzeteam.parser.web.AbstractWebParserFactory;
import com.cruzeteam.parser.web.Parser;
import taganrok.web.allorostov.AllorostovCategoriesParser;
import taganrok.web.allorostov.AllorostovItemsParser;
import taganrok.web.allorostov.AllorostovMainPageParser;
import taganrok.web.allorostov.AllorostovPagesParser;

/**
 * Created by Cruze on 20.07.2015.
 * factory http://www.8634city.ru/
 */
public class AllorostovFactory extends AbstractWebParserFactory {

    public static final String[] START_URL = {
            "http://allorostov.ru/"
    };

    @Override
    public String[] getStartLinks() {
        return START_URL;
    }

    @Override
    public Parser[] getParsers() {
        return new Parser[] {
                new AllorostovMainPageParser(),
                new AllorostovCategoriesParser(),
                new AllorostovPagesParser(),
                new AllorostovItemsParser()
        };
    }
}
