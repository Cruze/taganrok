package taganrok.web;

import com.cruzeteam.parser.web.AbstractWebParserFactory;
import com.cruzeteam.parser.web.Parser;
import taganrok.web.city8634.City8634ItemsParser;
import taganrok.web.city8634.City8634LinksParser;

/**
 * Created by Cruze on 20.07.2015.
 * factory http://www.8634city.ru/
 */
public class City8634Factory extends AbstractWebParserFactory {

    public static final String[] START_URL = {
            "http://www.8634city.ru/catalog/page/1000"
    };

    @Override
    public String[] getStartLinks() {
        return START_URL;
    }

    @Override
    public Parser[] getParsers() {
        return new Parser[] {
                new City8634ItemsParser(),
                new City8634LinksParser()
        };
    }
}
